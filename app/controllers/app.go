package controllers

import (
	log "github.com/inconshreveable/log15"
	gorm "github.com/revel/modules/orm/gorm/app"
	"github.com/revel/revel"
	"wg-manager/models"
)

type App struct {
	*revel.Controller
}

func (c App) connected() *models.User {
	if c.ViewArgs["email"] != nil {
		return c.ViewArgs["email"].(*models.User)
	}
	if email, ok := c.Session["email"]; ok {
		return c.getUser(email.(string))
	}
	return nil
}

func (c App) checkUser() revel.Result  {
	if user := c.connected(); user == nil {
		c.Flash.Error("Please login first")
		// return c.Redirect(App.Index)
	}
	return nil
}

func (c App) getUser(email string) (user *models.User)  {
	user = &models.User{}
	_, err := c.Session.GetInto("email", user, false)
	if user.Email == email || err != nil {
		return user
	}

	if err := gorm.DB.Where("email = ?", email).First(&user).Error; err != nil {
		log.Error(err.Error(), email)
	} else {
		log.Info("User found: "+user.Email)
		c.Session["email"] = user.Email
	}
	return
}

func (c App) Index() revel.Result {
	return c.Render()
}