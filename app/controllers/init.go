package controllers

import (
	log "github.com/inconshreveable/log15"
	gorm "github.com/revel/modules/orm/gorm/app"
	"github.com/revel/revel"
	bcrypt "golang.org/x/crypto/bcrypt"
	"wg-manager/models"
)

func initDB()  {
	// auto migrate db schemas
	gorm.DB.AutoMigrate(&models.User{})
	gorm.DB.AutoMigrate(&models.Role{})

	// create admin role
	var role = models.Role{}

	role.ID = 1
	role.Name = "admin"
	role.Description = "Administrator"

	err := gorm.DB.Where("id = ?", 1).First(&role).Error
	if err != nil {
		log.Info(err.Error())
		log.Info("Creating admin Role ...")
		gorm.DB.Create(&role)
	} else {
		log.Info("Admin role exist!")
	}

	// create admin user
	var user = models.User{}

	user.ID = 1
	user.Email = "admin@admin.com"
	user.Name = "admin"
	user.Password = "admin"
	user.HashedPassword, _ = bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	user.Active = true
	user.RoleRefer = 1

	err = gorm.DB.Where("id = ?", 1).First(&user).Error
	if err != nil {
		log.Info(err.Error())
		log.Info("Creating admin user ...")
		gorm.DB.Create(&user)
	} else {
		log.Info("Admin user exist!")
	}
}

func init()  {
	revel.InterceptMethod(App.checkUser, revel.BEFORE)
	revel.OnAppStart(initDB)
}