package models

import (
	"fmt"
	gorm "github.com/jinzhu/gorm"
	"github.com/revel/revel"
	"time"
)

type Role struct {
	gorm.Model
	Name string `gorm:"size:80;unique"`
	Description string `gorm:"size:255"`
}

type User struct {
	gorm.Model
	Email       string `gorm:"type:varchar(100);unique_index"`
	Name        string
	Password    string
	HashedPassword []byte
	Active		bool
	Role 		Role `gorm:"foreignkey:RoleRefer"`
	RoleRefer	int
	ConfirmedAt *time.Time `gorm:"type:datetime"`
}

func (user *User) String() string {
	return fmt.Sprintf("User(%s) - Email(%s)", user.Name, user.Email)
}

func (user *User) Validate(v *revel.Validation)  {
	v.Required(user.Email).Message("An email address is required!")
	v.Email(user.Email).Message("Pleas enter a valide email address!")
	v.Required(user.Name).Message("Name of the user is required!")
	v.Required(user.Password).Message("Pleas enter a password!")

	ValidatePassword(v, user.Password).Key("user.Password")
}

func ValidatePassword(v *revel.Validation, password string) *revel.ValidationResult  {
	return v.Check(password,
		           revel.Required{},
		           revel.MaxSize{16},
		           revel.MinSize{8},
	)
}